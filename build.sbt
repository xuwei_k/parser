scalaVersion := "2.11.4"

val scalazVersion = "7.1.0"

libraryDependencies ++= (
  ("org.scalaz" %% "scalaz-core" % scalazVersion) ::
  Nil
)

scalacOptions ++= (
  "-deprecation" ::
  "-unchecked" ::
  "-Xlint" ::
  "-language:existentials" ::
  "-language:higherKinds" ::
  "-language:implicitConversions" ::
  "-Ywarn-unused" ::
  "-Ywarn-unused-import" ::
  Nil
)
 
licenses := Seq("MIT License" -> url("http://www.opensource.org/licenses/mit-license.php"))
