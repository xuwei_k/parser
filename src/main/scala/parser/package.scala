import scalaz.IList
import scalaz.Id.Id

package object parser {

  type ParseError = String

  type Parser[S, A] = ParserT[Id, S, A]

  type OperatorTable[M[_], S, A] = IList[IList[Operator[M, S, A]]]
}