package parser

sealed abstract class Operator[M[_], S, A]

final case class Infix[M[_], S, A](run: ParserT[M, S, A => A => A], assoc: Assoc) extends Operator[M, S, A]

final case class Prefix[M[_], S, A](run: ParserT[M, S, A => A]) extends Operator[M, S, A]

final case class Postfix[M[_], S, A](run: ParserT[M, S, A => A]) extends Operator[M, S, A]
