package parser

import scalaz.IList

final case class SplitAccum[M[_], S, A](
  rassoc: IList[ParserT[M, S, A => A => A]],
  lassoc: IList[ParserT[M, S, A => A => A]],
  nassoc: IList[ParserT[M, S, A => A => A]],
  prefix: IList[ParserT[M, S, A => A]],
  postfix: IList[ParserT[M, S, A => A]]
)
