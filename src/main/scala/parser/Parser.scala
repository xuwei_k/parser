package parser

import scalaz._

final case class Result[S, A](input: S, result: ParseError \/ A, consumed: Boolean)

final case class ParserT[M[_], S, A](run: Kleisli[M, S, Result[S, A]]){
  def flatMap[B](f: A => ParserT[M, S, B])(implicit M: Monad[M]): ParserT[M, S, B] = ???
  def map[B](f: A => B): ParserT[M, S, B] = ???
}

sealed abstract class ParserTInstances0 {
  implicit def parserTFunctor[M[_]: Functor, S]: Functor[({type l[a] = ParserT[M, S, a]})#l] = ???
}

object ParserT extends ParserTInstances0 {
  def parserT[M[_], S, A](f: S => M[Result[S, A]]): ParserT[M, S, A] =
    new ParserT(Kleisli(f))

  implicit def parserTMonadPlus[M[_]: Monad, S]: MonadPlus[({type l[a] = ParserT[M, S, a]})#l] = ???
}

object Parser {

  def splitOp[M[_], S, A](op: Operator[M, S, A], accum: SplitAccum[M, S, A]): SplitAccum[M, S, A] =
    op match {
      case Infix(run, AssocNone) =>
        accum.copy(nassoc = run :: accum.nassoc)
      case Infix(run, AssocLeft) =>
        accum.copy(lassoc = run :: accum.lassoc)
      case Infix(run, AssocRight) =>
        accum.copy(rassoc = run :: accum.rassoc)
      case Prefix(run) =>
        accum.copy(prefix = run :: accum.prefix)
      case Postfix(run) =>
        accum.copy(postfix = run :: accum.postfix)
    }

  def rassocP[M[_], A, B, C, S](
    x: A,
    rassocOp: ParserT[M, S, A => A => A],
    prefixP: ParserT[M, S, B => C],
    term: ParserT[M, S, B],
    postfixP: ParserT[M, S, C => A]
  )(implicit M: Monad[M]): ParserT[M, S, A] = for{
    f <- rassocOp
    y <- for{
      z <- termP(prefixP, term, postfixP)
      * <- rassocP1(z, rassocOp, prefixP, term, postfixP)
    } yield *
  } yield f(x)(y)


  // rassocP1 :: forall m a b c s. (Monad m) => a -> ParserT s m (a -> a -> a) -> ParserT s m (b -> c) -> ParserT s m b -> ParserT s m (c -> a) -> ParserT s m a
  def rassocP1[M[_], A, B, C, S](
    x: A,
    rassocOp: ParserT[M, S, A => A => A],
    prefixP: ParserT[M, S, B => C],
    term: ParserT[M, S, B],
    postfixP: ParserT[M, S, C => A]
  ): ParserT[M, S, A] = ???


  def lassocP[M[_], A, B, C, S](
    x: A,
    lassocOp: ParserT[M, S, A => A => A],
    prefixP: ParserT[M, S, B => C],
    term: ParserT[M, S, B],
    postfixP: ParserT[M, S, C => A]
  )(implicit M: Monad[M]): ParserT[M, S, A] = for {
    f <- lassocOp
    y <- termP(prefixP, term, postfixP)
    * <- lassocP1(f(x)(y), lassocOp, prefixP, term, postfixP)
  } yield *

  // lassocP1 :: forall m a b c s. (Monad m) => a -> ParserT s m (a -> a -> a) -> ParserT s m (b -> c) -> ParserT s m b -> ParserT s m (c -> a) -> ParserT s m a
  // lassocP1 x lassocOp prefixP term postfixP = lassocP x lassocOp prefixP term postfixP <|> return x
  def lassocP1[M[_], A, B, C, S](
    x: A,
    lassocOp: ParserT[M, S, A => A => A],
    prefixP: ParserT[M, S, B => C],
    term: ParserT[M, S, B],
    postfixP: ParserT[M, S, C => A]
  )(implicit M: Monad[M]): ParserT[M, S, A] = {
    import syntax.plus._
    lassocP(x, lassocOp, prefixP, term, postfixP) <+> ParserT.parserTMonadPlus[M, S].point(x)
  }


  def termP[M[_], S, A, B, C](
    prefixP: ParserT[M, S, A => B],
    term: ParserT[M, S, A],
    postfixP: ParserT[M, S, B => C]
  )(implicit M: Monad[M]): ParserT[M, S, C] = for{
    pre <- prefixP
    x <- term
    post <- postfixP
  }yield post(pre(x))

  def consume[M[_], S](implicit M: Applicative[M]): ParserT[M, S, Unit] =
    ParserT.parserT(s => M.point(Result(s, \/-(()), consumed = true)))

  def fail[M[_], S, A](message: String)(implicit M: Applicative[M]): ParserT[M, S, A] =
    ParserT.parserT(s => M.point(Result(s, -\/(message), consumed = false)))
}