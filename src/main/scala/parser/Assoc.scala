package parser

sealed abstract class Assoc
case object AssocNone extends Assoc
case object AssocLeft extends Assoc
case object AssocRight extends Assoc

